#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Code snippets from Lecture 4

import traceback

# declaring and initialising variables
my_counter = 0
my_list = []

print(my_counter)

my_second_counter = my_counter + 1
print(my_second_counter)

# data types
print(5 + 10)         # legal
print("5 + 10")       # legal
print("cat" + "dog")  # legal
try:
    print("cat" + 5)      # illegal
except:
    traceback.print_exc()

# type conversion
answer = 42
sentence = "The answer is " + str(answer)
print(sentence)

# input function and type conversion
n = int(input("Enter a number between 1 and 10: "))
if not 1 <= n <= 10:
    print("The number you entered is not between 1 and 10")
print("You entered " + str(n))

age = input("Enter your age: ")    # age holds a string
print(type(age))
age = int(age)                     # age holds an integer
print(type(age))

# swapping two variables
var_a = 10
var_b = 5
print(var_a, var_b)
var_a, var_b = var_b, var_a
print(var_a, var_b)

# modification shortcuts
up_counter = 0
down_counter = 10
up_counter += 1
down_counter -= 1
print(up_counter, down_counter)

