#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# A substitution cipher is one of the earliest forms of cryptography
# in which a plaintext message is converted to ciphertext (i.e.,
# encrypted) by substituting letters via a lookup table. The
# ciphertext is decrypted by performing the reverse substitution.
#
# This code demonstrates the ROT13 substitution cipher which rotates
# letters by 13 places, i.e., "A" becomes "M", "B" becomes "N", etc.

import random

def encrypt_or_decrypt(input_message):
    """Encrypts or decrypts a message in ROT13."""
    output_message = ''
    for ch in input_message.upper():
        if str.isalpha(ch):
            output_message += chr(ord('A') + (ord(ch) - ord('A') + 12) % 26)
        else:
            output_message += ch

    return output_message


# encode a simple message
message = "Hello World!!!"
print(message)

coded_message = encrypt_or_decrypt(message)
print(coded_message)

decoded_message = encrypt_or_decrypt(coded_message)
print(decoded_message)
